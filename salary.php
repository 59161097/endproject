<?php

session_start();
// include('')
require_once "connection.php";



?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .checked {
            color: orange;
        }
    </style>
    <!-- <link rel="stylesheet" href="admin_c/css/adminstyle.css"> -->


</head>

<body>
    <header style=" padding: 3rem;
    background: #333;
    text-align: center;
    color: #fff;
    font-size: 3rem;
    
    ">

        <h1>EZ-MOVE
        </h1>


    </header>
    <section class="content" >
        <div class="content__grid container">

            <div class="showinfo">
                <div class="box-content">
                    <h1 style="background-color: yellow;">ดูความเห็นผู้ใช้บริการ/ค่าตอบแทน</h1>
                </div>

                <div class="" stlye="font: size 100px;">
                    <div class="row">
                        <div class="col-8"></div>
                        <div class="col-1">
                            <h4>ค้นหา</h4>
                        </div>

                        <div class="col-2">
                            <input class="input" type="text" id="myInput" onkeyup="myFunction()" placeholder="พิมพ์ค้นหา....">
                        </div>
                    </div>
                    <?php

                    $eid  =  $_SESSION['name'];


                    // $sql2 = "SELECT user_book.*, user_applicate.car_image 
                    // from user_book  join user_applicate on  user_book.bID=user_applicate.id
                    // /// เงื่อนไขใหม่
                    $sql = "SELECT user_book.* , user_applicate.car_image 
                         FROM user_book join user_applicate on  user_book.bID=user_applicate.id
                          WHERE b_driver_name=:eid ORDER BY id DESC ";



                    $query = $db->prepare($sql);
                    $query->bindParam(':eid', $eid, PDO::PARAM_STR);
                    $query->execute();
                    $results = $query->fetchAll(PDO::FETCH_OBJ);


                    $cnt = 1;


                    if ($query->rowCount() > 0) {
                        foreach ($results as $row) {               ?>

                            <table id="myTable" class="table table-bordered table-striped   " style="border:solid 5px; background-color:  #fff; font-size: 20px; ">


                                <!-- ***********    หัวตาราง ***********    หัวตาราง ***********    หัวตาราง ***********    หัวตาราง -->
                                <thead style="background-color: orange;">
                                    <tr>
                                        <th>No.</th>
                                        <th>หมายเลขสมัคร</th>
                                        <th>ประเภทรภ</th>
                                        <th>รูปรถ</th>
                                        <th>ความเห็นผู้ใช้ริการ</th>
                                    </tr>
                                </thead>

                                <!-- // ************** tr ***********// ************** tr ***********// ************** tr *********** -->
                                <tr>
                                    <td class="text-center"><?php echo htmlentities($cnt); ?></td>

                                    <td class="font-w600"><?php echo htmlentities($row->b_BookingID); ?></td>

                                    <td class="font-w600"><?php echo htmlentities($row->b_car_type); ?></td>

                                    <!-- **** ดึงภาพ ใหม่ -->
                                    <td class="font-w600"><img src="upload_car/<?php echo $row->car_image ?>" width="250px" height="250px" height="100px" alt=""></td>


                                    <td style="background-color:lightyellow;"><?php echo htmlentities($row->message2); ?> </td>
                                    <thead>
                                        <tr>
                                            <th colspan="2">ชื่อผู้ใช้บริการ</th>
                                            <th colspan="2">คะแนนจากผู้ใช้บริการ</th>
                                            <th colspan="1">ค่าตอบแทนตามเปอร์เซนต์(60%)</th>
                                        </tr>
                                    </thead>
                                <tr>
                                    <td colspan="2"><?php echo htmlentities($row->b_hire_name); ?> </td>

                                    <td colspan="2">
                                        <?php if ($row->star == 5) { ?>

                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>

                                        <?php } else  if ($row->star == 4) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>

                                        <?php } else if ($row->star == 3) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($row->star == 2) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($row->star == 1) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else { ?>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } ?>
                                    </td>
                                    <td colspan="1"><?php echo htmlentities((60 * ($row->b_price)) / 100); ?> บาท</td>
                                </tr>

                        <?php $cnt = $cnt + 1;
                        }
                    } ?>

                            </table>

                </div>

            </div>
        </div>
        <div class="columns" style="padding:4px">
            <div class="column text-center">

                <a href="home.php" class="btn btn-success">กลับสู่หน้าหลัก</a>

            </div>
        </div>

    </section>

    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> --> -->

    <script>
        $(document).ready(function() {
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


</body>




</html>