<?php
session_start();
require_once('connection.php');


$idna = $_SESSION['user_login'];   // ส่งจากหน้า login




if (!isset($_SESSION['user_login'])) {

    header("location: index.php");
}


// $tmp=$_SESSION['user_login'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EZ_MOVE - หน้าแรก</title>
    <link rel="shortcut icon" type="image/png" href="./resources/imgs/logo.png" />

    <!-- Utilities CSS-->
    <link href="./resources/css/color.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/icon.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/page-helper.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/element.css" type="text/css" rel="stylesheet">

    <!--Commons CSS -->
    <link href="./resources/css/initial.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/navbar.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/main-menu.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/custom.css" type="text/css" rel="stylesheet">

    <!--JS-->
    <script src="./resources/js/Controller_page.js"></script>
    <script src="./resources/js/accController.js"></script>
    <script src="./resources/js/index.js"></script>

    <!--Bootstrap -->
    <!-- <link rel="stylesheet" href="resources/css/bootstrap.min2.css" type="text/css"> -->
    <!--Custome Style -->
    <!-- <link rel="stylesheet" href="resources/css/style2.css" type="text/css"> -->
</head>

<body style="background-color:yellow;">


    <div class="navbar-div">
        <nav class="navbar " aria-label="main navigation" style=" background-color: black!important;">

            <a class="navbar-item banner">
                <div>
                    <img class="navbar-banner-logo" src="./resources/icons/index_icons/EZ-MOVE (1).png" alt="logo"></img>

                    <span class="navbar-banner-text"> EZ-MOVE</span>
                </div>
            </a>
            <button id="navbar-user" class="navbar-item user" onclick="switchNavBarDropDown()">
                <div style="background-color:red" class="user-container">
                    <svg class="navbar-user-icon icon-user icon-size-5"></svg>
                    <span class="navbar-user-text"><?php echo $_SESSION['user_login']; ?></span>
                    <svg class="navbar-user-icon icon-down-arrow icon-size-6"></svg>
                </div>
                <div id="navbar-dropdown" class="dropdown-items">


                    <a href="logout.php">Logout</a>

                </div>
            </button>
        </nav>
    </div>


    <!-- ****แทรกข้าง -->
    <!-- <div class="columns"> -->
    <!-- <div class="column two-tone-one is-orange">
                <br><br><br>
                <p class="font-title"><b>หน้าแรก</b></p>
                <div class="column text-center">
                    <img src="./resources/icons/index_icons/EZ-MOVE (1).png">
                </div>
            </div> -->
    <br><br>

    <div style="width:100% ;   ">

        <div class="subcontent-main-div index" style=" background-color: #fdba00d2 !important;">

            <div class=" box with-title is-round " style="background-color: floralwhite ">
                <div class="box-title" style="font-size:50px; background-color: #fdba00d2 !important;"> หน้าแรก </div>
                <div class="box-content">


                    <div>


                        <h1>ข้อมูลส่วนตัว </h1>
                    </div>
                    <?php if (isset($_SESSION['success'])) : ?>
                        <div class="alert alert-success">
                            <h3>
                                <?php
                                echo $_SESSION['success'];
                                echo $_SESSION['user_login'];
                                echo  $_SESSION['user_id'];

                                unset($_SESSION['success']);
                                ?>
                            </h3>

                        <?php endif ?>

                        <!-- ************ ทำกรอบสวยๆด้วย -->
                        <div class=" box text-center" style="width: 100;">

                            <?php
                            $sql = "SELECT * from usertbls where u_name=:idna ";
                            $query = $db->prepare($sql);
                            $query->bindParam(':idna', $idna, PDO::PARAM_STR);
                            $query->execute();
                            $results = $query->fetchAll(PDO::FETCH_OBJ);
                            //  *****************sql**************


                            if ($query->rowCount() > 0) {
                                foreach ($results as $row) {

                                    $id_na = $row->id;
                                    $img_na = $row->img;
                                    $name_na = $row->name;
                                    $_SESSION['name'] = $row->name;
                                    $tel_na = $row->tel;
                                    $email = $row->email;
                                }
                            }




                            ?>

                            <div class="row" style="border: double 5px;;">
                                <div style="margin-left: 80px;" class="col  text-center">
                                    <!-- <img style="width:200px ;" src="upload_car/<?php echo htmlentities($result->car_image); ?>" class="img-responsive" alt="Image" /> </a> -->

                                    <img style="width:300px" src="upload_person/<?php echo htmlentities($img_na) ?>">

                                </div>


                                <table style="text-align:left; padding-left:30%;padding-top:50px;font-size: 30px; display:block;">

                                    <tr style="border:solid; background-color:yellow;">
                                        <td style="padding:5px;width: 50%;">
                                            ชื่อ     :
                                        </td>
                                        <td >
                                            <?php echo htmlentities($name_na); ?>
                                        </td>
                                    </tr>
                                    <tr style="border:solid;background-color:lightgrey;">
                                    <td style="padding:5px;width: 50%;">
                                            ยูเซอร์เนม :
                                        </td>
                                        <td>
                                            <?php echo htmlentities($_SESSION['user_login']); ?>
                                        </td>
                                    </tr>
                                    <tr style="background-color:yellow;">
                                    <td style="padding:5px;width: 50%;">
                                            เบอร์โทร :
                                        </td>
                                        <td>
                                            <?php echo htmlentities($tel_na) ?>
                                        </td>
                                    </tr>
                                    <tr style="background-color:lightgrey;">
                                    <td style="padding:5px;width: 50%;">
                                            เมลล์ :
                                        </td>
                                        <td>
                                            <?php echo htmlentities($email) ?>
                                        </td>
                                    </tr>
                                </table>
                                <!-- <h2> ชื่อ : <?php echo htmlentities($name_na); ?> </h2> -->

                                <!-- <div class="form-group">
                                        <h2> ยูเซอร์เนม : <?php echo htmlentities($_SESSION['user_login']); ?> </h2>
                                    </div> -->


                                <!-- <h2> เบอร์โทร : <?php echo htmlentities($tel_na) ?> </h2> -->

                                <!-- <h2>เมลล์ <?php echo htmlentities($email) ?> </h2> -->

                            </div>

                        </div>
                        <hr>

                        <!-- ******************************* -->

                        <div>
                            <h1>บริการของเรา
                            </h1>
                        </div>
                        <div class="columns">
                            <a class="column menu-box text-center" href="user_booking.php ">
                                <!-- <a class="column menu-box text-center" href="car_list.php "> -->
                                <img src="./resources/icons/index_icons/car-color.svg">
                                <h3 class="label is-2">จัดการจองรถ</h3>
                            </a>

                            <a class="column menu-box text-center" href="car_list_3.php">
                                <img class="navbar-banner-logo" src="./resources/icons/index_icons/car-booking.png" alt="logo"></img>
                                <h3 class="label is-2">ดูสถานะจองรถ/ค่าใช้จ่าย</h3>
                            </a>


                            <a class="column menu-box text-center" href="satisfic.php">

                                <img style="width: 200px" src="./resources/icons/index_icons/satis2.png">
                                <h3 class="label is-2">ประเมินความพอใจบริการ</h3>
                            </a>

                        </div>

                        <h1>ร่วมงานกับเรา</h1>

                        <div class="columns">
                            <a class="column menu-box text-center" href="applicate_job.php  ">

                                <img src="./resources/icons/index_icons/id-card.svg">
                                <h3 class="label is-2">สมัครงาน</h3>
                            </a>
                            <a class="column menu-box text-center" href="post_app_st.php">
                                <img src="./resources/icons/index_icons/motor.svg">
                                <h3 class="label is-2">ดูสถานะสมัครงาน</h3>
                            </a>
                            <a class="column menu-box text-center" href="salary.php">
                                <img src="./resources/icons/index_icons/crypto.png">
                                <h3 class="label is-2">ดูสถานะอนุมัติเงิน</h3>
                            </a>
                        </div>

                        </div>
                </div>
            </div>

        </div>
</body>

<footer>
    <!-- Controller JS -->
    <script src="./resources/js/menuController.js"></script>
    <script src="./resources/js/navbarController.js"></script>

    <style>
        * {
            box-sizing: border-box;
        }

        /* Create two unequal columns that floats next to each other */
        .col {
            float: left;
            padding: 10px;
        }

        .l1 {
            width: 25%;
        }

        .l2 {
            width: 50%;
        }

        .l3 {
            width: 20%;
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
    </style>
</footer>

</html>

<?php
