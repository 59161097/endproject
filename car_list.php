<?php
session_start();
require_once('connection.php');
$st = 'อนุมัติแล้ว';



?>

<!DOCTYPE HTML>
<html lang="en">

<head>

    <title>Car Rental Portal | Car Listing</title>
    <!--Bootstrap -->
    <link rel="stylesheet" href="resources/css/bootstrap.min2.css" type="text/css">
    <!--Custome Style -->
    <link rel="stylesheet" href="resources/css/style2.css" type="text/css">

    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .checked {
            color: orange;
        }
    </style>

</head>
<!-- **** อยากให้ font this page is bigger ****** -->

<body style="background-color: lightyellow; ">


    <?php include('includes/header.php'); ?>
    <!-- /Page Header-->


    <!--Listing-->
    <section class="listing-page">
        <div class="container">
            <!-- **************ใจกลาง***************** -->


            <?php
            $st = 'อนุมัติแล้ว';
            $username = $_SESSION['user_login'];
            $busy = 'ว่าง';
            //Query for Listing count
            // $sql = "SELECT id from user_applicate where status=:st ";
            $sql = "SELECT user_applicate.*, usertbls.name,  usertbls.tel ,usertbls.id as bid  from user_applicate join usertbls on usertbls.id=user_applicate.ServiceID 
                        where status=:st AND  u_name <>:uname and busy=:busy 
                        -- and zone=:zone 
                        ";

            $query = $db->prepare($sql);
            $query->bindParam(':st', $st, PDO::PARAM_STR);
            $query->bindParam(":uname", $username);
            $query->bindParam(":busy", $busy);
            // $query->bindParam(':zone',   $_POST['select_geo']);

            $query->execute();
            $results = $query->fetchAll(PDO::FETCH_OBJ);
            $cnt = $query->rowCount();
            ?>
            <div class="col-md-9 col-md-push-3">
                <!-- *******list************ -->
                <div class="result-sorting-wrapper">
                    <div class="sorting-count">



                        <!--**************** ******9 Listing************** -->
                        <h2 style="color: blue;">มีรายการทั้งหมด <?php echo htmlentities($cnt); ?> ลิสต์</h2>

                        <!-- *********car**************************** -->

                    </div>
                </div>


                <?php

                //  $sql = "SELECT tblvehicles.*, tblbrands.BrandName,tblbrands.id as bid  from tblvehicles join tblbrands on tblbrands.id=tblvehicles.VehiclesBrand";
                if (isset($_POST['btn_search'])) {
                    // $_POST['txt_BT'];
                    // $_POST['select_geo'];
                    $c_t = $_POST['select_car'];
                    $sql = "SELECT user_applicate.*, usertbls.name,  usertbls.tel ,usertbls.id as bid  from user_applicate join usertbls on usertbls.id=user_applicate.ServiceID where status=:st 
                    AND  u_name <>:uname AND car_type=:c_t and busy=:busy 
                    -- and zone=:zone 
                     " ;
                    $query = $db->prepare($sql);
                    // $query->bindParam(':zone',   $_POST['select_geo']);
                    $query->bindParam(':c_t', $c_t);
                    $query->bindParam(':busy', $busy);
                    $query->bindParam(':st', $st, PDO::PARAM_STR);
                    $query->bindParam(":uname", $username);
                    $query->execute();
                    $results = $query->fetchAll(PDO::FETCH_OBJ);
                } else if (isset($_POST['btn_search_all'])) {
                    // $_POST['txt_BT'];
                    // $_POST['select_geo'];
                    $sql = "SELECT user_applicate.*, usertbls.name,  usertbls.tel ,usertbls.id as bid  from user_applicate join usertbls on usertbls.id=user_applicate.ServiceID 
                    where status=:st AND  u_name <>:uname  and busy=:busy 
                    -- and zone=:zone  
                    ";
                    $query = $db->prepare($sql);
                    $query->bindParam(':st', $st, PDO::PARAM_STR);
                    // $query->bindParam(':zone',   $_POST['select_geo']);
                    $query->bindParam(":uname", $username);
                    $query->bindParam(':busy', $busy);
                    $query->execute();
                    $results = $query->fetchAll(PDO::FETCH_OBJ);
                }



                if ($query->rowCount() > 0) {
                    foreach ($results as $result) {  ?>

                        <div class="product-listing-m gray-bg" style="padding: top 10px;margin:top 10px">
                            <div class="product-listing-img">
                                <img style="width:200px ; height: 200px;" src="upload_car/<?php echo htmlentities($result->car_image); ?>" class="img-responsive" alt="Image" /> </a>
                            </div>


                            <div class="product-listing-content-h5">

                                <h6 style="font-size: 25px;">ประเภทรถ : <?php echo htmlentities($result->car_type); ?></h6>

                                <?php $_SESSION['cartype'] = $result->car_type; ?>


                                <ul>
                                    <?php



                                    $sql3 = "SELECT user_book.* , user_applicate.car_image 
FROM user_book join user_applicate on  user_book.bID=user_applicate.id
WHERE b_driver_name=:eid and b_car_type=:car_t
ORDER BY id DESC ";



                                    $q_ment = $db->prepare($sql3);
                                    $q_ment->bindParam(':eid', $result->name, PDO::PARAM_STR);
                                    $q_ment->bindParam(':car_t', $result->car_type, PDO::PARAM_STR);
                                    $q_ment->execute();
                                    $res = $q_ment->fetchAll(PDO::FETCH_OBJ);

                                    ?>
                                    <li style="font-size: 20px;">
                                        เขตโซนบริการ : ภาค<?php  ?>
                                    </li>

                                    <li style="font-size: 20px;">ชื่อผู้ขับ : <?php echo htmlentities($result->name); ?> </li>

                                    <li style="font-size: 20px;"> โทร :<?php echo htmlentities($result->tel); ?> </li>

                                    <li style="font-size: 20px;"> ความนิยม :

                                        <?php if ($result->avg_star == 5) { ?>

                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>

                                        <?php } else  if ($result->avg_star  == 4) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>

                                        <?php } else if ($result->avg_star  == 3) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($result->avg_star  == 2) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($result->avg_star  == 1) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else { ?>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } ?>
                                        (รีวิว : <?php echo htmlentities($q_ment->rowCount()); ?> ครั้ง)
                                    </li>



                                </ul>

                                <!-- ****ดูรายละเอียด**** -->
                                <?php
                                if ($result->busy == "ว่าง") {
                                ?>
                                    <a href="car_list_2.php?vhid=<?php echo htmlentities($result->id); ?>" class="btn">ดูรายละเอียด </span></a>


                                <?php } ?>


                            </div>

                        </div>

                <?php }
                } ?>

            </div>

            <!-- **************หมดใจกลาง***************** -->

            <!-- *** <aside  Find Your Car </h5>****************** -->
            <!--Side-Bar-->
            <aside class="col-md-3 col-md-pull-9" style="background-color: white;">
                <div class="sidebar_widget">

                    <!-- *****หัว Find Your Car -->
                    <div class="widget_heading">
                        <h5><i class="fa fa-filter" aria-hidden="true"></i> ค้นหาประเภทรถ</h5>
                    </div>
                    <div class="sidebar_filter">
                        <!-- **************  ********  <form a**>Select Brand********* -->
                        <form action="" method="post">
                            <div class="form-group select">
                                <!-- ******** ตัวแปร name="select_car" ********* -->
                                <select name="select_car" class="form-control">
                                <option value="ทั้งหมด">ทั้งหมด</option>
                                <option value="จักรยานยนต์">จักรยานยนต์</option>
                                <option value="รถปิคอัพ">รถปิคอัพ</option>
                                <option value="รถกระบะทึบ">กระบะทึบ</option>
                                <option value="รถ 6 ล้อ">รถ 6 ล้อ</option>
                                <option value="รถ 10 ล้อ">รถ 10 ล้อ</option>

                            </select>
                        </div>


                                </select>
                            </div>

                            <!-- *******************************form find your car** -->


                            <div class="form-group">
                                <!-- <button type="submit" class="btn btn-block"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</button> -->
                                <input type="submit" name="btn_search" class="btn btn-block" value="ค้นหา">
                                <input type="submit" name="btn_search_all" class="btn btn-block " value="ดูทั้งหมด">

                            </div>
                        </form>
                    </div>
                </div>


                <!-- ******end recent   *************** -->

            </aside>
            <!--/Side-Bar-->


        </div>
        <!--/contain-->
    </section>
    <!-- /Listing-->

</body>

</html>