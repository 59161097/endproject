<?php

require_once('connection.php');
session_start();
$idna = $_SESSION['user_login'];   // ส่งจากหน้า login

if (isset($_POST['btn_insert'])) {
    try {



        // $bid = mt_rand(100000000, 999999999);
        // $bid = $_GET['booking'];
        // $bid =$id;  ใช้ไม่ได้ ต้อง feath array อยุ่ดี
        $bookingid = mt_rand(100000000, 999999999);
        $car_type = $_POST['txt_car_type'];
        $car_years = $_POST['txt_car_years'];
        $bank_name = $_POST['txt_bank_name'];
        $bank_id = $_POST['txt_bank_id'];

        $image_file_car = $_FILES['txt_file_car']['name'];
        $type = $_FILES['txt_file_car']['type'];
        $size = $_FILES['txt_file_car']['size'];
        $temp = $_FILES['txt_file_car']['tmp_name'];

        $image_file_cerr = $_FILES['txt_file_cerr']['name'];
        $type_cerr = $_FILES['txt_file_cerr']['type'];
        $size_cerr = $_FILES['txt_file_cerr']['size'];
        $temp_cerr = $_FILES['txt_file_cerr']['tmp_name'];

        // $image_file_person = $_FILES['txt_file_person']['name'];
        // $type_person = $_FILES['txt_file_person']['type'];
        // $size_person = $_FILES['txt_file_person']['size'];
        // $temp_person = $_FILES['txt_file_person']['tmp_name'];



        //    $path_cerr = "upload_cerr/" . $image_file_cerr; // set upload folder path
        //         $path_person = "upload_person/" . $image_file_person; // set upload folder path
        $path = "upload_car/" . $image_file_car; // set upload folder path


        if (empty($car_type)) {
            $errorMsg = "Please Enter car_type";
        } else if (empty($car_years)) {
            $errorMsg = "please Select car_years";
        } else if (empty($bank_name)) {
            $errorMsg = "please Select bank_name";
        } else if (empty($bank_id)) {
            $errorMsg = "please Select bank_id";
        } else if (empty($image_file_car)) {
            $errorMsg = "please Select Image";
        } else if ($type == "image/jpg" || $type == 'image/jpeg' || $type == "image/png" || $type == "image/gif") {
            if (!file_exists($path)) { // check file not exist in your upload folder path
                if ($size < 5000000) { // check file size 5MB
                    move_uploaded_file($temp, 'upload_car/' . $image_file_car);
                    move_uploaded_file($temp_cerr, 'upload_cerr/' . $image_file_cerr);
                    // move_uploaded_file($temp_person, 'upload_person/' . $image_file_person);

                    // move upload file temperory directory to your upload folder
                } else {
                    $errorMsg = "Your file too large please upload 5MB size"; // error message file size larger than 5mb
                }
            } else {
                $errorMsg = "Car File already exists... Check upload filder"; // error message file not exists your upload folder path
            }
        } else {
            $errorMsg = "Upload JPG, JPEG, PNG & GIF file formate...";
        }
        //  *******"SELECT * **********sql**************
        $sql = "SELECT * from usertbls where u_name=:idna ";
        $query = $db->prepare($sql);
        $query->bindParam(':idna', $idna, PDO::PARAM_STR);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_OBJ);
        if (!isset($errorMsg)) {

            if ($query->rowCount() > 0) {
                foreach ($results as $row) {
                    $busy = 'ว่าง';
                    $id_na = $row->id;
                    // ****sql******* insert ****************
                    // *************อ้าง GET booking
                    // $insert_stmt = $db->prepare('INSERT INTO user_applicate(BookingID, certificate_image, person,account_id, account_name, car_years, car_image, car_type, ServiceID) VALUES (:bookingid, :fcerr, :fperson , :fbankid,:fbankname,:fyears, :fimage, :fcartype, :bid)');
                    // *************ไม่อ้าง GET booking
                    $insert_stmt = $db->prepare('INSERT INTO  user_applicate(busy, ServiceID, BookingID, certificate_image, person,account_id, account_name, car_years, car_image, car_type , zone) 
                    VALUES (:busy, :id_na, :bookingid, :fcerr, :fperson , :fbankid,:fbankname,:fyears, :fimage, :fcartype, :zone)');
                    $insert_stmt->bindParam(':id_na', $id_na);
                    $insert_stmt->bindParam(':bookingid', $bookingid, PDO::PARAM_STR);
                    $insert_stmt->bindParam(':fyears', $car_years);
                    $insert_stmt->bindParam(':fcartype', $car_type);
                    $insert_stmt->bindParam(':fimage', $image_file_car);
                    $insert_stmt->bindParam(':fbankname', $bank_name);
                    $insert_stmt->bindParam(':fbankid', $bank_id);
                    $insert_stmt->bindParam(':fcerr', $image_file_cerr);
                    $insert_stmt->bindParam(':fperson', $image_file_person);
                    $insert_stmt->bindParam(':busy', $busy);
                    $insert_stmt->bindParam(':zone', $_POST['txt_zone']);

                    if ($insert_stmt->execute()) {


                        $insertMsg = "ดีใจด้วย!!!!!...อัพโหลดไฟล์สำเร็จ...";
                        header('refresh:3;home.php');
                    }
                }
            }
        }
    } catch (PDOException $e) {
        $e->getMessage();
    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" type="image/png" href="./resources/imgs/logo.png" />

    <!--Commons CSS -->
    <link href="./resources/css/initial.css" type="text/css" rel="stylesheet">

    <!-- Utilities CSS-->
    <link href="./resources/css/color.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/icon.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/page-helper.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/element.css" type="text/css" rel="stylesheet">

    <!--Commons CSS -->
    <link href="./resources/css/initial.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/navbar.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/main-menu.css" type="text/css" rel="stylesheet">
    <link href="./resources/css/custom.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

</head>

<body>

    <div class="columns">
        <div class="column two-tone-one is-orange" style=" background-color: #fdba00d2 !important;">
            <br><br><br>
            <p class="font-title"><b>สมัครงานกับเรา</b></p>
            <div class="column text-center">
                <img src="./resources/icons/register.png" alt="logo">
            </div>
        </div>
        <div class="columns two-tone-two">

            <div class="column">

                <div class="box border-gray is-rounded" style="margin:60px">



                    <div> <?php
                            if (isset($errorMsg)) {
                            ?>
                            <div class="alert alert-danger">
                                <strong><?php echo $errorMsg; ?></strong>
                            </div>
                        <?php } ?>

                        <?php
                        if (isset($insertMsg)) {
                        ?>
                            <div class="alert alert-success">
                                <strong><?php echo $insertMsg; ?></strong>
                            </div>
                        <?php } ?>

                        <!--  หลายตัวแปรenctype="multipart/form-data"> -->
                        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <div class="columns ">

                                <div class="column text-center form-group" style="padding:9px">
                                    <h4>เลือกสาย</h4>
                                        <select name="txt_zone" class="column text-center form-group my-input" required>
                                            <option value="" selected="selected">- สายรถ -</option>
                                            <option value="เหนือ">เหนือ</option>
                                            <option value="กลาง">กลาง</option>
                                            <option value="ใต้">ใต้</option>
                                            <option value="อีสาน">อีสาน</option>

                                        </select>
                                    </select>
                                </div>
                                <div class="column text-center form-group" style="padding:9px">
                                    <h4>เลือกประเภทรถ</h4>
                                    <select name="txt_car_type" class="column text-center form-group my-input">
                                        <option value="" selected="selected">- เลือกประเภทรถ -</option>
                                        <option value="จักรยานยนต์">จักรยานยนต์</option>
                                        <option value="รถปิคอัพ">รถปิคอัพ</option>
                                        <option value="รถกระบะทึบ">รถกระบะทึบ</option>
                                        <option value="รถ 6 ล้อ">รถ 6 ล้อ</option>
                                        <option value="รถ 10 ล้อ">รถ 10 ล้อ</option>
                                    </select>
                                </div>
                                <div class="column text-center form-group" style="padding:9px">
                                    <h4>อายุของรถ(ปี)</h4>
                                    <input class="my-input" type="text" name="txt_car_years">
                                </div>
                            </div>

                            <h4>อัพโหลดภาพรถ</h4>
                            <div class="column form-group " style="padding:9px">

                                <input type="file" name="txt_file_car" class="my-input">
                            </div>
                            <h4>อัพโหลดใบขับขี่</h4>
                            <div class="column form-group" style="padding:9px">
                                <input type="file" name="txt_file_cerr" class="my-input">
                            </div>
                            <!-- <h4>อัพโหลดรูปถ่ายหน้าตรง</h4>
                            <div class="column form-group" style="padding:9px">
                                <input type="file" name="txt_file_person" class="my-input">
                            </div> -->
                    </div>


                    <h4>กรอกชื่อธนาคาร</h4> <br>
                    <div class="column form-group" style="padding:9px">
                        <input type="text" name="txt_bank_name" class="my-input">
                    </div>
                    <h4>กรอกบัญชีธนาคาร</h4>
                    <div class="column form-group" style="padding:9px">
                        <input type="text" name="txt_bank_id" class="my-input">

                    </div>


                    <div class="columns" style="padding:9px">
                        <div class="column text-center" style="padding:9px">

                            <input type="submit" name="btn_insert" class="button margin-left-x is-round is-2 width80 is-oros" value="ตกลง">
                        </div>
                        <div class="column text-center" style="padding:9px">
                            <a href="home.php"><button type="button" class="button margin-left-x is-round is-2 width80 is-yentafo">
                                    ยกเลิก</button></a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
</body>

</html>



<!-- บัตรประจำตัวประชาชน
ใบขับขี่รถยนต์ หรือ ใบขับขี่รถจักรยานยนต์
รูปถ่ายหน้าตรง ไม่สวมหมวก หรือ แว่นดำ (รูปนี้จะปรากฏที่หน้าแอปเมื่อผู้โดยสารเรียกรถ)
สมุดบัญชีธนาคารกสิกรไทย

ทำว่าถ่ายรวม 

เล่มจดทะเบียนรถยนต์ หรือ เล่มจดทะเบียนรถจักรยานยนต์
พรบ.รถยนต์ หรือ พรบ.รถจักรยานยนต์ (ที่ยังไม่หมดอายุ)
ชื่อและเบอร์โทรศัพท์บุคคลที่ติดต่อได้ในกรณีฉุกเฉิน -->