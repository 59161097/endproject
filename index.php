<?php

session_start();

if (isset($_SESSION['admin_login'])) {
	header("location: admin_c/index.php");
}



if (isset($_SESSION['user_login'])) {
	header("location: home.php");
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>EZ-MOVe - สมัครสมาชิก</title>


	<link rel="shortcut icon" type="image/png" href="./resources/imgs/logo.png" />

	<!--Commons CSS -->
	<link href="./resources/css/initial.css" type="text/css" rel="stylesheet">

	<!-- Utilities CSS-->
	<link href="./resources/css/color.css" type="text/css" rel="stylesheet">
	<link href="./resources/css/icon.css" type="text/css" rel="stylesheet">
	<link href="./resources/css/page-helper.css" type="text/css" rel="stylesheet">
	<link href="./resources/css/element.css" type="text/css" rel="stylesheet">

	<!--Commons CSS -->
	<link href="./resources/css/initial.css" type="text/css" rel="stylesheet">
	<link href="./resources/css/navbar.css" type="text/css" rel="stylesheet">
	<link href="./resources/css/main-menu.css" type="text/css" rel="stylesheet">
	<link href="./resources/css/custom.css" type="text/css" rel="stylesheet">

	<script src="./resources/js/Controller_page.js"></script>


	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body style="background-color: yellow;">

	<div class="columns">
		<div class="column two-tone-one " style=" background-color: #fdba00d2 !important;">
			<br><br><br>
			<p class="font-title"><b>เข้าสู่ระบบ</b></p>
			<div class="column text-center">
				<img src="./resources/icons/index_icons/EZ-MOVE (1).png" alt="logo" width="498px" height="498px"></img>

				<!-- <img src="./resources/icons/login.png" alt="logo" width="498px" height="498px"> -->
			</div>
		</div>
		<div class="columns two-tone-two">
			<div class="column">
				<div class="box border-gray is-rounded" style=" margin:60px ">
					<div>
						<div class="column text-center">
							<img src="./resources/icons/log.png" width="60px" height="60px">
						</div>

						<?php if (isset($_SESSION['success'])) : ?>
							<div class="alert alert-success">
								<h3>
									<?php
									echo $_SESSION['success'];
									unset($_SESSION['success']);
									?>
								</h3>
							</div>
						<?php endif ?>

						<?php if (isset($_SESSION['error'])) : ?>
							<div class="alert alert-danger">
								<h3>
									<?php
									echo $_SESSION['error'];
									unset($_SESSION['error']);
									?>
								</h3>
							</div>
						<?php endif ?>


						

						<form action="login_db.php" method="post" class="form-horizontal my-5">

							<div class="column" style="padding:9px">
								<input class="my-input" type="text" name="txt_uname" placeholder="ใส่ Username" required>
							</div>
							<br>
							<div class="column" style="padding:9px">
								<input class="my-input" type="password" name="txt_password" placeholder="ใส่ Password" required>
							</div>
							<br>
							<div class="column" style="padding:9px">
								<div class="form-group">
									<label for="type" class="col-sm-3 control-label">เลือกประเภท Login</label>
									<div class="col-sm-12">
										<select name="txt_role" class="my-input" required>
											<option value="" selected="selected">- เลือกประเภท -</option>
											<option value="admin">Admin</option>

											<option value="user">User</option>
										</select>
									</div>
								</div>
							</div>
					</div>

					<div class="columns" style="padding:4px">
						<div class="column text-center">


							<input type="submit" name="btn_login" class="btn btn-success" style="width: 100%;" value="  เข้าสู่ระบบ">

						</div>
					</div>

					<div class="" style="padding:4px   ">
						<div class="">
							<div class="" style=" text-align: center">
								------------------------------------
								<p><a href="register.php">ไปสมัครสมาชิก</a></p>
							</div>
						</div>
					</div>

					</form>

				</div>
			</div>
		</div>


		<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>