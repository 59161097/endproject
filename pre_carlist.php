<?php
session_start();
require_once('connection.php');

?>

<!DOCTYPE HTML>
<html lang="en">

<head>

    <title>Car Rental Portal | Car Listing</title>
    <!--Bootstrap -->
    <link rel="stylesheet" href="resources/css/bootstrap.min2.css" type="text/css">
    <!--Custome Style -->
    <link rel="stylesheet" href="resources/css/style2.css" type="text/css">

    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        .checked {
            color: orange;
        }
    </style>

</head>
<!-- **** อยากให้ font this page is bigger ****** -->

<body style="background-color: lightyellow; ">


    <?php include('includes/header.php'); ?>
    <!-- /Page Header-->

    <!--Listing-->
    <section class="listing-page">
        <div class="container">
            <!-- **************ใจกลาง***************** -->
            <?php
            if (($_POST['txt_car_type']) == 'ทั้งหมด') {  ?>
                <?php
                $st = 'อนุมัติแล้ว';
                $username = $_SESSION['user_login'];
                // $busy = 'ว่าง';
                $c_t = $_POST['txt_car_type'];
                $zone = $_POST['txt_zone'];
                //Query for Listing count
                // $sql = "SELECT id from user_applicate where status=:st ";
                // $_POST['txt_BT'];
                $sql = "SELECT user_applicate.*, usertbls.name,  usertbls.tel ,usertbls.id as bid  from user_applicate 
                join usertbls on usertbls.id=user_applicate.ServiceID 
                where status=:st  AND  u_name <>:uname  and zone=:zone  ";
                $query = $db->prepare($sql);
                $query->bindParam(':zone', $zone);
                // $query->bindParam(':busy', $busy);
                $query->bindParam(':st', $st, PDO::PARAM_STR);
                $query->bindParam(":uname", $username);


                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);
                $cnt = $query->rowCount();
                ?>



                <div style="background-color: yellow;" class="row result-sorting-wrapper">
                    <div class="col l2 sorting-count">

                        <!--**************** ******9 Listing************** -->
                        <h2 style="color: blue;">มีรายการทั้งหมด <?php echo htmlentities($cnt); ?> ลิสต์</h2>
                        <h3 style="color: ">สายบริการรถ: <?php echo $_POST['txt_zone'] ?></h3>
                        <h3 style="color: ">วันจองรถ: <?php echo $_POST['txt_BT'] ?></h3>
                        <h3 style="color: ">ประเภทรถ: <?php echo $_POST['txt_car_type'] ?></h3>

                        <!-- *********car**************************** -->
                    </div>
                    <div class="col l3">
                        <div>
                            <img width="200" height="150"  src="./resources/icons/index_icons/EZ-MOVE (1).png" alt="logo" width="498px" height="498px"></img>
                        </div>
                        <div >
                            <a href="user_booking.php" style="width:200px;background-color: red;color:blanchedalmond;font-size:30px" class="btn">
                                กลับ</a>
                        </div>
                    </div>
                </div>

                <?php

                if ($query->rowCount() > 0) {
                    foreach ($results as $result) {  ?>

                        <div class="product-listing-m gray-bg" style="padding: top 10px;margin:top 10px">
                            <div class="product-listing-img">
                                <img style="width:200px ; height: 200px;" src="upload_car/<?php echo htmlentities($result->car_image); ?>" class="img-responsive" alt="Image" /> </a>
                            </div>


                            <div class="product-listing-content-h5">

                                <h6 style="font-size: 25px;">ประเภทรถ : <?php echo htmlentities($result->car_type); ?></h6>

                                <?php $_SESSION['cartype'] = $result->car_type; ?>


                                <ul>
                                    <?php


$idle='เสร็จ';
                                    $sql3 = "SELECT user_book.* , user_applicate.car_image 
FROM user_book join user_applicate on  user_book.bID=user_applicate.id
WHERE b_driver_name=:eid and b_car_type=:car_t and idle=:idle
ORDER BY id DESC ";



                                    $q_ment = $db->prepare($sql3);
                                    $q_ment->bindParam(':eid', $result->name, PDO::PARAM_STR);
                                    $q_ment->bindParam(':car_t', $result->car_type, PDO::PARAM_STR);
                                    $q_ment->bindParam(':idle', $idle, PDO::PARAM_STR);
                                    $q_ment->execute();
                                    $res = $q_ment->fetchAll(PDO::FETCH_OBJ);

                                    ?>


                                    <li style="font-size: 20px;">ชื่อผู้ขับ : <?php echo htmlentities($result->name); ?> </li>

                                    <li style="font-size: 20px;"> โทร :<?php echo htmlentities($result->tel); ?> </li>

                                    <li style="font-size: 20px;"> ความนิยม :

                                        <?php if ($result->avg_star == 5) { ?>

                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>

                                        <?php } else  if ($result->avg_star  == 4) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>

                                        <?php } else if ($result->avg_star  == 3) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($result->avg_star  == 2) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($result->avg_star  == 1) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else { ?>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } ?>
                                        (รีวิว : <?php echo htmlentities($q_ment->rowCount()); ?> ครั้ง)
                                    </li>



                                </ul>

                                <!-- ****ดูรายละเอียด**** -->

                                <a href="car_list_2.php?vhid=<?php echo htmlentities($result->id); ?>" class="btn">ดูรายละเอียด </span></a>





                            </div>

                        </div>

                <?php }
                } ?>


                <!-- **************หมดใจกลาง***************** -->

            <?php     } else { ?>
                <?php
                $st = 'อนุมัติแล้ว';
                $username = $_SESSION['user_login'];
                // $busy = 'ว่าง';
                $c_t = $_POST['txt_car_type'];
                $zone = $_POST['txt_zone'];
                //Query for Listing count
                // $sql = "SELECT id from user_applicate where status=:st ";
                // $_POST['txt_BT'];
                $sql = "SELECT user_applicate.*, usertbls.name,  usertbls.tel ,usertbls.id as bid  from user_applicate 
                join usertbls on usertbls.id=user_applicate.ServiceID 
                where status=:st  AND  u_name <>:uname AND car_type=:c_t and zone=:zone  ";
                $query = $db->prepare($sql);
                $query->bindParam(':zone', $zone);
                $query->bindParam(':c_t', $c_t);
                // $query->bindParam(':busy', $busy);
                $query->bindParam(':st', $st, PDO::PARAM_STR);
                $query->bindParam(":uname", $username);


                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);
                $cnt = $query->rowCount();
                ?>



                <div style="background-color: yellow;" class="result-sorting-wrapper">
                    <div class="sorting-count">

                        <!--**************** ******9 Listing************** -->
                        <h2 style="color: blue;">มีรายการทั้งหมด <?php echo htmlentities($cnt); ?> ลิสต์</h2>
                        <h3 style="color: ">สายบริการรถ: <?php echo $_POST['txt_zone'] ?></h3>
                        <h3 style="color: ">วันจองรถ: <?php echo $_POST['txt_BT'] ?></h3>
                        <h3 style="color: ">ประเภทรถ: <?php echo $_POST['txt_car_type'] ?></h3>

                        <!-- *********car**************************** -->
                        <div>
                            <a href="user_booking.php" style="background-color: red;color:blanchedalmond;font-size:30px" class="btn">
                                กลับ</a>
                        </div>
                    </div>
                </div>

                <?php

                if ($query->rowCount() > 0) {
                    foreach ($results as $result) {  ?>

                        <div class="product-listing-m gray-bg" style="padding: top 10px;margin:top 10px">
                            <div class="product-listing-img">
                                <img style="width:200px ; height: 200px;" src="upload_car/<?php echo htmlentities($result->car_image); ?>" class="img-responsive" alt="Image" /> </a>
                            </div>


                            <div class="product-listing-content-h5">

                                <h6 style="font-size: 25px;">ประเภทรถ : <?php echo htmlentities($result->car_type); ?></h6>

                                <?php $_SESSION['cartype'] = $result->car_type; ?>


                                <ul>
                                    <?php



                                    $sql3 = "SELECT user_book.* , user_applicate.car_image 
FROM user_book join user_applicate on  user_book.bID=user_applicate.id
WHERE b_driver_name=:eid and b_car_type=:car_t
ORDER BY id DESC ";



                                    $q_ment = $db->prepare($sql3);
                                    $q_ment->bindParam(':eid', $result->name, PDO::PARAM_STR);
                                    $q_ment->bindParam(':car_t', $result->car_type, PDO::PARAM_STR);
                                    $q_ment->execute();
                                    $res = $q_ment->fetchAll(PDO::FETCH_OBJ);

                                    ?>


                                    <li style="font-size: 20px;">ชื่อผู้ขับ : <?php echo htmlentities($result->name); ?> </li>

                                    <li style="font-size: 20px;"> โทร :<?php echo htmlentities($result->tel); ?> </li>

                                    <li style="font-size: 20px;"> ความนิยม :

                                        <?php if ($result->avg_star == 5) { ?>

                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>

                                        <?php } else  if ($result->avg_star  == 4) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>

                                        <?php } else if ($result->avg_star  == 3) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($result->avg_star  == 2) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else if ($result->avg_star  == 1) { ?>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } else { ?>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star "></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        <?php } ?>
                                        (รีวิว : <?php echo htmlentities($q_ment->rowCount()); ?> ครั้ง)
                                    </li>



                                </ul>

                                <!-- ****ดูรายละเอียด**** -->

                                <a href="car_list_2.php?vhid=<?php echo htmlentities($result->id); ?>" class="btn">ดูรายละเอียด </span></a>





                            </div>

                        </div>

                <?php }
                } ?>

            <?php     } ?>
        </div>

        <!--/contain-->
    </section>
    <!-- /Listing-->

</body>

</html>

<style>
    * {
        box-sizing: border-box;
    }

    /* Create two unequal columns that floats next to each other */
    .col {
        float: left;
        padding: 10px;
    }

    .l1 {
        width: 25%;
    }

    .l2 {
        width: 75%;
    }

    .l3 {
        width: 20%;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }
</style>