<?php

session_start();
// include('')
require_once "../connection.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>
    <link rel="stylesheet" href="css/adminstyle.css">

</head>

<body>
    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>
    <section class="content">
        <div class="content__grid">
            <?php include('nav.php'); ?>

            <div class="showinfo">
                <h1 style=" background-color:  #ecfd00!important;">ดูรายการรถทั้งหมด</h1>
                <div class="row">
                    <div class="col-8"></div>
                    <div class="col-1">
                        <h4>ค้นหา</h4>
                    </div>

                    <div class="col-2">
                        <input class="input" type="text" id="myInput" onkeyup="myFunction()" placeholder="พิมพ์ค้นหา....">
                    </div>
                </div>
                <div>
                    <?php
                    $sql = "SELECT user_applicate.*,usertbls.name FROM user_applicate join usertbls on user_applicate.ServiceID=usertbls.id    ";


                    $query = $db->prepare($sql);

                    $query->execute();
                    $results = $query->fetchAll(PDO::FETCH_OBJ);


                    $cnt = 1;


                    if ($query->rowCount() > 0) {
                        foreach ($results as $row) {               ?>
                            <table id="myTable" style="font-size: 20px;border:solid;" class="table ">

                                <tr>
                                    <th>No.</th>
                                    <th>ฺประเภทรถ</th>
                                    <th>ชื่อเจ้าของรถ</th>
                                    <th>สถานะรถ</th>
                                    <th>Action</th>
                                </tr>
                                <!-- ***********    หัวตาราง ***********    หัวตาราง ***********    หัวตาราง ***********    หัวตาราง -->


                                <!-- // ************** tr ***********// ************** tr ***********// ************** tr *********** -->
                                <tr>
                                    <td class="text-center"><?php echo htmlentities($cnt); ?></td>


                                    <!-- *  id จาก user_applicate -->
                                    <td> <?php echo htmlentities($row->car_type); ?> </td>


                                    <td>
                                        <span class=""><?php echo htmlentities($row->name); ?></span>
                                    </td>


                                    <td>
                                        <?php if ($row->busy == 'ว่าง') { ?>
                                            <span style="background-color: green;color:white;font-size:20px;" class="">ว่าง</span>
                                        <?php } else if ($row->busy == 'ไม่ว่าง') { ?>
                                            <span style="background-color: red;color:white;font-size:20px" class="">ไม่ว่าง</span>
                                        <?php } ?>
                                    </td>

                                    <!-- / //**************************************************editid ส่ง $_GET['editid'];ต่อหน้า book detail******************td -->
                                    <td class="d-none d-sm-table-cell"><a href="app-detail.php?editid=<?php echo htmlentities($row->id); ?>">รายละเอียดรถ</a></td>

                                </tr>

                                <tr>
                                    <th colspan="2">ภาพ</th>
                                    <td colspan="2">
                                        <img style="width: 150px;margin-right:150px" src="../upload_car/<?php echo $row->car_image; ?>">

                                    </td>
                                    <td colspan="1">

                                        <div class="form-group">
                                            <a href="hire2.php?b_id=<?php echo $row->b_ID ?> &amp; hire=<?php echo $row->hire_name ?>&amp;
                                             app_id=<?php echo $row->id ?> &amp; c_cnt=<?php echo $row->cnt ?>">
                                             คิวผู้ที่ใช้บริการ
                                             </a>
                                        </div>

                                        <?php if ($row->busy == 'ไม่ว่าง') { ?>
                                            <div class="form-group">
                                                <a href="hire.php?b_id=<?php echo $row->b_ID ?> &amp; hire=<?php echo $_SESSION['hhr']=$row->hire_name ?>&amp; app_id=<?php echo $row->id ?>">ผู้ที่กำลังใช้บริการ</a>
                                            </div>
                                        <?php } ?>
                                    </td>





                                </tr>

                        <?php $cnt = $cnt + 1;
                        }
                    } ?>
                            </table>
                </div>


            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</body>

</html>

<script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- 
<div class="subcontent-area">
        <div class="subcontent-main-div index box-set-width">
            <div class="box with-title is-round">
                
                <div class="box-content">
                    
                    <div class="columns">
                        <div class="column">
                            <p id="totalExpenses" class="font-total-price"></p>                              
                            <p id="totalIncome" class="font-total-price"></p>   
                            <p id="totalProfit" class="font-total-profit"></p>                             
                        </div>
                        <div class="column is-profit-1">
                                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->