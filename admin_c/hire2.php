<?php

session_start();
// include('')
require_once "../connection.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>
    <link rel="stylesheet" href="css/adminstyle.css">

</head>

<body>
    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>
    <section class="content">
        <div class="content__grid">
            <?php include('nav.php'); ?>

            <div class="showinfo">
                <h1 style=" background-color:  #ecfd00!important;">คิวที่กำลังใช้บริการรถ (เรียงจากวันจอง)</h1>
                <?php

                //*** ดึงที่น้อย */  **** ใช้ไม่ได้เพราะเทียบรวม table sql หมด
                // $sql2 = "SELECT MIN(user_book.FromDate) as min FROM  usertbls join user_book
                //  on usertbls.u_name=user_book.b_hire_name  
                //   where bID=:bid  ";
                // $q = $db->prepare($sql2);
                // $q->bindParam(':bid', $_GET['app_id'], PDO::PARAM_STR);
                // $q->execute();
                // $r = $q->fetch(PDO::FETCH_ASSOC);
                // // $row = $select_stmt->fetch(PDO::FETCH_ASSOC);
                // $min = $r['min'];





                //*** หลัก  */
            

                $b_status = 'อนุมัติแล้ว';
                // echo $hire;
                // echo $bid;
                $sql = "SELECT usertbls.name,usertbls.img,usertbls.u_name,
                usertbls.tel,usertbls.email,user_book.* FROM  usertbls join user_book 
                 on usertbls.u_name=user_book.b_hire_name   where bID=:bid and b_status=:bst 
                 ORDER BY user_book.FromDate ASC ";
                // $sql = "SELECT * FROM user_applicate   where user_applicate.hire_name=:hire  ";


                $query = $db->prepare($sql);
                $query->bindParam(':bid', $_GET['app_id'], PDO::PARAM_STR);
                $query->bindParam(':bst',  $b_status, PDO::PARAM_STR);

                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);


                $cnt = 1;

                // ** */ แสดงหลายรายการ .sizeof()  ไปหามา

                if ($query->rowCount() > 0) {
                    foreach ($results as $row) {

                        $us_name = $row->name;
                        $us_img = $row->img;
                        $us_uname = $row->u_name;
                        $us_tel = $row->tel;
                        $us_email = $row->email;


                ?>

                        <h1><?php echo $_SESSION['hhr'] ?></h1>

                        <div>

                            <table class="table " style="font-size: 20px;border:double;">

                                <th> 
                                <h1 style="background-color:yellow">คิวที่ <?php echo $cnt ?> </h1>
                                    <img style="width:200px ;" src="../upload_person/<?php echo  $us_img ?>">
                                </th>

                                <td>

                                    <table>


                                        <tr>
                                            <th>username</th>
                                            <td>
                                                <?php echo htmlentities($us_uname); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>ชื่อ นามสกุล</th>
                                            <td>
                                                <?php echo htmlentities($us_name); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>เบอร์โทร</th>
                                            <td>
                                                <?php echo htmlentities($us_tel); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>อีเมลล์</th>
                                            <td>
                                                <?php echo htmlentities($us_email); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>วันที่จองรถ</th>

                                            <td>
                                                <span style="font-size:30px" class="badge badge-primary">
                                                    <?php echo htmlentities($row->FromDate); ?>
                                                </span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <div style="margin-top: 50px;" class="form-group">
                                        <span class="">
                                            <a style="color: white;background-color:blue" href="book_detail.php?bid=<?php echo  $row->id ?>">ดูรายละเอียดจองรถ</a>
                                        </span>

                                        <h1><?php  echo $row->id   ?> </h1>
                                        <h1>app id<?php  echo  $_GET['app_id']  ?> </h1>
                                    </div>
                                    <br>
                                    <hr style="border: solid;"><br>
                                    <!-- //** ต้องมี เงื่นไข วันน้อยที่สุด ดักไว้ */ -->
                                    <?php if ( $row->idle == '' && $_REQUEST['c_cnt'] == $cnt) { ?>
                                        <div>
                                            <a  href="hire2.php?idbook=<?php  echo $row->id   ?> &amp; c_cnt=<?php  echo $cnt   ?>
                                            &amp;  ap_id=<?php echo $_GET['app_id']?> ">ไปอนุมัติ</a>
                                            <!-- <input style="background-color: green; color:white" type="submit" name="btn" value="อนุมัติส่งเสร็จสิ้น" onclick="return confirm('คุณแน่ใจเหรอว่า จะอนุมัติ!!');"> -->
                                        </div>
                                    <?php } else if ($row->idle == 'เสร็จ') { ?>
                                        <h1 style="background-color:green"> เสร็จแล้ว</h1>
                                    <?php } else if ($row->idle == 'กำลังใช้บริการ') { ?>
                                        <h1 style="background-color:yellow;"> กำลังใช้บริการ</h1>
                                    <?php } ?>

                                </td>

                            </table>

                        </div>

                <?php $cnt = $cnt + 1;
                    }
                }  ?>


            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</body>

</html>

<?php
if (isset($_REQUEST['idbook'])) {
    $bid = $_GET['app_id'];
    $ccc=$_GET['c_cnt'];
    $sss=$ccc+1;
    //** UPDATE busy=:busy */

    // $usq = "UPDATE   user_applicate SET  WHERE  id=:app_id";
    // $update = $db->prepare($usq);
    // $update->bindParam(':busy', $busy, PDO::PARAM_STR);
    // $update->bindParam(':hire', $hire$_GET['hire'];, PDO::PARAM_STR);
    // $update->bindParam(':app_id', $_SESSION['b'], PDO::PARAM_STR);   // select from user_book
    // $update->execute();

    $idle = 'กำลังใช้บริการ';
    $sq = "UPDATE user_book SET idle=:idle WHERE  id=:app_id";
    $sq = $db->prepare($sq);
    $sq->bindParam(':app_id', $_GET['idbook'], PDO::PARAM_STR);
    $sq->bindParam(':idle', $idle, PDO::PARAM_STR);
    $sq->execute();

    // **  UPDATE user_applicate SET busy=:busy */
    $busy = 'ไม่ว่าง';  // รออนุมัต้
    $usq = "UPDATE user_applicate SET busy=:busy , hire_name=:hire, cnt=:cnt WHERE  id=:app_id";
    $update = $db->prepare($usq);
    $update->bindParam(':busy', $busy);
    $update->bindParam(':cnt',$sss );
    $update->bindParam(':hire',$_SESSION['hhr']);
    $update->bindParam(':app_id', $_GET['ap_id'], PDO::PARAM_STR);   // select from user_book
    $update->execute();
    echo "<script>alert('อนุมัติ..รายการนี้แล้ว!!!! ');</script>";
    echo "<script type='text/javascript'> document.location = 'car_app.php'; </script>";
                    
}
?>