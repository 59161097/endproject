<?php


session_start();
require_once "../connection.php";


$pid = $_GET['u_name'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>
    <link rel="stylesheet" href="css/adminstyle.css">

</head>

<body>
    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>
    <section class="content">
        <div class="content__grid">
            <?php include('nav.php'); ?>
            <div class="showinfo">
                <h1 style=" background-color:  #ecfd00!important;">ดูคำร้องการสมัคร ทั้งหมด</h1>
                <?php

                $sql = "SELECT user_book.*, user_applicate.car_image,user_applicate.car_type,user_applicate.car_years,user_applicate.b_Regdate
                from user_book  join user_applicate on  user_book.bID=user_applicate.id
                Where user_book.b_hire_name=:pid    
                 order by user_book.id desc";
                // $sql = "SELECT * FROM  user_book join usertbls on user_applicate.ServiceID=usertbls.id where usertbls.id=:pid   ";


                $query = $db->prepare($sql);
                $query->bindParam(':pid', $pid, PDO::PARAM_STR);

                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);


                $cnt = 1;


                if ($query->rowCount() > 0) {
                    foreach ($results as $row) {               ?>
                        <table class="table " style="font-size: 20px;border:double;">

                            <th>
                                <img style="width:200px ;" src="../upload_car/<?php echo htmlentities($row->car_image); ?>">
                            </th>

                            <td>

                                <table>

                                    <tr>
                                        <th>no</th>
                                        <td class=""><?php echo htmlentities($cnt); ?></td>
                                    </tr>

                                    <tr>
                                        <th>หนังสืออนุมัติ</th>


                                        <?php if ($row->b_status == 'อนุมัติแล้ว') { ?>
                                            <td>
                                                <span style="background-color: green; color:white; display:block; text-align:center" class="">
                                                    อนุมัติแล้ว
                                                </span>
                                            </td>

                                        <?php } else if ($row->b_status == 'ไม่อนุมัติ') { ?>
                                            {<td>
                                                <span style="background-color: tomato; color:white ;display:block; text-align:center" class="">
                                                    ไม่อนุมัติแล้ว
                                                </span>
                                            </td>
                                            }
                                        <?php } else if ($row->b_status == 'รอ') { ?>
                                            {<td>
                                                <span style="background-color: blue; color:white ;display:block; text-align:center" class="">
                                                    กำลังรอ...อนุมัติ
                                                </span>
                                            </td>
                                            }
                                        <?php } ?>


                                    </tr>
                                    <tr>
                                        <th>สถานะงาน</th>


                                        <?php if ($row->idle == 'เสร็จ') { ?>
                                            <td>
                                                <span style="background-color: green; color:white;display:block; text-align:center" class="">
                                                    เสร็จสิ้น
                                                </span>
                                            </td>

                                        <?php } else if ($row->idle == 'กำลังใช้บริการ') { ?>
                                            {<td>
                                                <span style="background-color: blue; color:white;display:block; text-align:center" class="">
                                                   กำลังดำเนินการ
                                                </span>
                                            </td>
                                            }
                                        <?php } else if ($row->idle == '') { ?>
                                            {<td>
                                                <span style="background-color: tomato; color:white;display:block; text-align:center" class="">
                                                    กำลังรอ...คิวใช้บริการ
                                                </span>
                                            </td>
                                            }
                                        <?php } ?>


                                    </tr>
                                    <tr>
                                        <th>ประเภทรถ</th>
                                        <td>
                                            <?php echo htmlentities($row->b_car_type); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>อายุรถ</th>
                                        <td>
                                            <?php echo htmlentities($row->car_years); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>วันที่ทำรายการ</th>
                                        <td>
                                            <span class="badge badge-primary"><?php echo htmlentities($row->bb_Regdate); ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="background-color:yellow ;padding-top: 150px; ">
                            <span style="background-color: red; display:block ; text-align:center ">
                            
                                        <a style="color:white;" href="book_detail.php?bid=<?php echo $row->id ?>">ดูรายละเอียดคำร้อง</a>
                            </span>
                            </td>
                        </table>
                <?php $cnt = $cnt + 1;
                    }
                } ?>

            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</body>

</html>
<!-- 
<div class="subcontent-area">
        <div class="subcontent-main-div index box-set-width">
            <div class="box with-title is-round">
                
                <div class="box-content">
                    
                    <div class="columns">
                        <div class="column">
                            <p id="totalExpenses" class="font-total-price"></p>                              
                            <p id="totalIncome" class="font-total-price"></p>   
                            <p id="totalProfit" class="font-total-profit"></p>                             
                        </div>
                        <div class="column is-profit-1">
                                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->