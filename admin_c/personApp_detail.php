<?php

use function PHPSTORM_META\type;

session_start();
// include('')
require_once "../connection.php";




?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>EZ-MOVE -หน้าคำร้อง มาใหม่</title>
    <link rel="stylesheet" href="css/adminstyle.css">

</head>

<body>
    <header>
        <div class="container">
            <h1>Welcome to Admin Page </h1>
        </div>
    </header>
    <section class="content">
        <div class="content__grid">
            <?php include('nav.php'); ?>
            <div class="showinfo">
                <h1 style=" background-color:  #ecfd00!important;">ดูคำร้องการสมัคร ทั้งหมด</h1>
                <?php

                $pid = $_GET['id'];
                $sql = "SELECT *,user_applicate .id FROM  user_applicate join usertbls on usertbls.id=user_applicate.ServiceID where usertbls.id=:pid   ";


                $query = $db->prepare($sql);
                $query->bindParam(':pid', $pid, PDO::PARAM_STR);

                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);


                $cnt = 1;


                if ($query->rowCount() > 0) {
                    foreach ($results as $row) {               ?>
                        <table class="table " style="font-size: 20px;border:double;">

                            <th>
                                <img style="width:200px ;" src="../upload_car/<?php echo htmlentities($row->car_image); ?>">
                            </th>

                            <td>

                                <table>

                                    <tr>
                                        <!-- <th>No.</th> -->
                                        <th>no</th>
                                        <td class="text-center"><?php echo htmlentities($cnt); ?></td>
                                    </tr>

                                    <tr>
                                        <th>สถานะ</th>


                                        <?php if ($row->status == 'อนุมัติแล้ว') { ?>
                                            <td>
                                                <span style="background-color: green; color:white" class="">
                                                    อนุมัติแล้ว
                                                </span>
                                            </td>

                                        <?php } else if($row->status == 'ไม่อนุมัติ') { ?>
                                            {<td>
                                                <span style="background-color: tomato; color:white" class="">
                                                    ไม่อนุมัติแล้ว
                                                </span>
                                            </td>
                                            }
                                        <?php } ?>


                                    </tr>
                                    <tr>
                                        <th>ประเภทรถ</th>
                                        <td>
                                            <?php echo htmlentities($row->car_type); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>อายุรถ</th>
                                        <td>
                                            <?php echo htmlentities($row->car_years); ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th>วันที่ทำรายการ</th>

                                        <td>
                                            <span class="badge badge-primary"><?php echo htmlentities($row->u_Regdate); ?></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="background-color: yellow;">
                                    <div style=" margin-top: 70px;" class="form-group">
                                        <span class="">
                                            <a style="style="margin-left:1px; color: white;background-color:blue" href="app-detail.php?editid=<?php echo  htmlentities($row->id) ?>">ดูรายละเอียด</a>
                                        </span>
                                    </div>
                                  
                                </td>
                        </table>
                <?php $cnt = $cnt + 1;
                    }
                } ?>

            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</body>

</html>
<!-- 
<div class="subcontent-area">
        <div class="subcontent-main-div index box-set-width">
            <div class="box with-title is-round">
                
                <div class="box-content">
                    
                    <div class="columns">
                        <div class="column">
                            <p id="totalExpenses" class="font-total-price"></p>                              
                            <p id="totalIncome" class="font-total-price"></p>   
                            <p id="totalProfit" class="font-total-profit"></p>                             
                        </div>
                        <div class="column is-profit-1">
                                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->